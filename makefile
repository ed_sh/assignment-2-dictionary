ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: all clean run_% test
all: program
	echo build complete

clean:
	$(RM) *.o main

run_%: %
	./$<

test: test.py program
	python3 test.py


%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc words.inc

words.inc: colon.inc

program: main.o lib.o dict.o
	$(LD) -o $@ $^



