%macro externize 1-*
%rep %0
extern %1
%rotate 1
%endrep
%endmacro

externize exit, string_length, print_string, print_err, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy
