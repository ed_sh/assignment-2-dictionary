import subprocess

tests = [
    {"in": "name", "out": "Edward", "err": ""},
    {"in": "birthdate", "out": "10.06.2004", "err": ""},
    {"in": "not_exist", "out": "", "err": "Element was not found"},
    {"in": "", "out": "", "err": "Element was not found"},
    {"in": "a"*300, "out": "", "err": "Input word is too large"}
]

passed = True
print("Testing...")
for test in tests:
    with subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as program:

        stdout, stderr = program.communicate(test["in"].encode())

        stdout = stdout.decode().strip()
        stderr = stderr.decode().strip()

        if(program.returncode == -11):
            print("❌", end="")
            passed = False
            test["error"] = f'Segmentation fault for input "{test["in"]}"'
            continue
        
        if (stdout == test["out"] and stderr == test["err"]):
            print("✅", end="")
            continue

        passed = False
        print("❌", end="")
        test["error"] = f'Wrong output for input "{test["in"]}":'
        if(stdout != test["out"]):
            test["error"] += f'\n    stdout: "{stdout}" ("{test["out"]}" expected)'
        if(stderr != test["err"]):
            test["error"] += f'\n    stderr: "{stderr}" ("{test["err"]}" expected)'

print("\n")
if passed:
    print("All tests passed!")
else:
    for test in tests:
        if("error" in test):
            print(test["error"])