%include "lib.inc"

global find_word

section .text

; rdi - 0-sym string; rsi - dict begin pointer; ret found element / 0 if not
find_word:
    xor rax, rax
    push r12 ; callee-saving
    push r13
    mov r12, rdi ; key to find
    mov r13, rsi ; in-dict pointer

.finding:
    test r13, r13 ; if dict pointer empty -
    jz .exit ; return 0
    lea rdi, [r13+8] ; get current addr of key
    mov rsi, r12 ; string to compare
    call string_equals
    test rax, rax ; strings eq - return current pointer
    jnz .retcurr
    ; if 0 - not equals
    mov r13, qword [r13] ; switch cur pointer to next
    jmp .finding

.retcurr:
    mov rax, r13
.exit:
    pop r13
    pop r12
    ret