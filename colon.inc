%define FirstSign 0 ; a pointer to first element, default empty

%macro colon 2
%2: ; put sign
dq FirstSign ; put last sign adress
db %1, 0 ; put key-string + 0-sym
%define FirstSign %2 ; redefine last sign
%endmacro
