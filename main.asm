%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%define BUF_LEN 256

section .rodata
overflow_err: db 'Input word is too large', 10, 0
not_found_err: db 'Element was not found', 10, 0

section .bss
input: resb BUF_LEN

section .text
global _start
_start:
    mov rdi, input ; buf addr
    mov rsi, BUF_LEN ; buf size
    call read_word
    test rax, rax
    jz .overflow ; word overflow
    push rdx ; save string key len

    mov rdi, rax ; key to find
    mov rsi, FirstSign ; pointer to first element
    call find_word ; lets find it
    pop rdx ; get key length
    test rax, rax ; found?
    jz .not_found

    lea rdi, [rax + 8 + rdx + 1]  ; addr of value: found addr + 8 bytes of next element addr + key string len + 0-sym of key string
    call print_string
    call print_newline
    jmp .exit

.overflow:
    mov rdi, overflow_err
    call print_err
    jmp .exit

.not_found:
    mov rdi, not_found_err
    call print_err

.exit:
    xor rdi, rdi
    call exit